#!/bin/bash -xe
sudo yum -y install python3-pip git
git clone https://gitlab.com/ttwthomas/mcflurry /home/ec2-user/mcflurry
cd mcflurry
sudo pip3 install -r requirements.txt